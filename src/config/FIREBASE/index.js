import firebase from 'firebase';

firebase.initializeApp({
  apiKey: "AIzaSyBN45PHYbNrawn4WsaIOidU29G78ZbdAZI",
  authDomain: "ar-shooping.firebaseapp.com",
  databaseURL: "https://ar-shooping-default-rtdb.firebaseio.com",
  projectId: "ar-shooping",
  storageBucket: "ar-shooping.appspot.com",
  messagingSenderId: "1050457331253",
  appId: "1:1050457331253:web:10ff73649ef1ed7018837c",
  measurementId: "G-20J2SC9YTH"
});

const FIREBASE = firebase;

export default FIREBASE
