export const colors = {
    primary: '#A30606',
    white: '#FFFFFF',
    secondary: '#C39595',
    yellow: '#FFF6D5',
    border: '#C4C4C4'
}